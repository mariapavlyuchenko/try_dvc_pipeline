import numpy as np
import json
import pandas as pd
import sys

def compute_stats(csv_path):
    """
        Compute 2 statictics from a csv file:
        mean, standard deviation
    """
    csv_file_df = pd.read_csv(csv_path, sep=',')
    stats_dict = {
        "mean": np.mean(csv_file_df['numbers']),
        "std": np.std(csv_file_df['numbers'])
    }
    return stats_dict


if __name__ == '__main__':
    f_in = sys.argv[1]
    f_out = sys.argv[2]
    stats_dict = compute_stats(f_in)
    with open(f_out, "w") as f:
        json.dump(stats_dict, f)



