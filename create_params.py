import yaml

if __name__ == '__main__':
    data = dict(
        generaterandom=dict(
            n=100,
            seed=1000)
    )

    with open('params.yaml', 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False)