import pandas as pd
import sys


def messap(csv_path):
    """
        Multiply numbers from data.csv on 2
    """
    csv_file_df = pd.read_csv(csv_path, sep=',')
    csv2_file_df = csv_file_df['numbers']*2
    return csv2_file_df


if __name__ == '__main__':
    f_in = sys.argv[1]
    f_out = sys.argv[2]
    messap(f_in).to_csv(f_out)


