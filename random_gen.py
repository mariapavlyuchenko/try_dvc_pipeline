import numpy as np
import pandas as pd
import yaml
import sys

def randomize(random_seed, n_samples):
    """
        Create CSV file with random data with params:
        random_seed, n_samples
    """
    np.random.seed(random_seed)
    data = np.random.random(n_samples)
    df = pd.DataFrame({'numbers': data[:]})
    return df


if __name__ == '__main__':
    f_out = sys.argv[1]
    with open("params.yaml", 'r') as fd:
        params = yaml.safe_load(fd)
    n_samples = params['generaterandom']['n']
    random_seed = params['generaterandom']['seed']

    df = randomize(random_seed, n_samples)
    df.to_csv(f_out)

