import json
import sys


def summarize(files):
    objs = []
    for filename in files:
        with open(filename) as f:
            file = json.load(f)
            objs.append(file)
    return objs


if __name__ == '__main__':
    files = [sys.argv[1], sys.argv[2]]
    f_out = sys.argv[3]
    with open(f_out, "w") as f:
        json.dump(summarize(files), f)





